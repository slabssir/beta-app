<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Home extends AbstractController
{
    /**
     * @Route("/home", name="index")
     */
    public function index()
    {
        return $this->json(['name' => 'hello']);
    }
}
